export const environment = {
  production: false,
  getConsultor:{
    url: 'http://localhost:666/getConsultor',
    timeOutMiliSeconds: 5000
  },

  getMetrics:{
    url: 'http://localhost:666/getMetrics',
    timeOutMiliSeconds: 5000
  }
};