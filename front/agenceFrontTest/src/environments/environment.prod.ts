export const environment = {
  production: true,
  getConsultor:{
    url: 'http://18.222.199.30:8080/AgenceBU/getConsultor',
    timeOutMiliSeconds: 5000
  },

  getMetrics:{
    url: 'http://18.222.199.30:8080/AgenceBU/getMetrics',
    timeOutMiliSeconds: 5000
  }
};
