import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import Swal from 'sweetalert2';
import { GetMetricsService } from '../services/getMetrics.servoce';
import { SpinnerService } from '../services/spinner.service';

@Component({
  selector: 'app-consultor-page',
  templateUrl: './consultor-page.component.html',
  styleUrls: ['./consultor-page.component.css']
})
export class ConsultorPageComponent implements OnInit {
  
  public data = [];
  static datosConsulta: any;

  constructor(
    private router: Router,
    private spinner: SpinnerService,
    private metrics: GetMetricsService
  ) { }

  ngOnInit(): void {
    $(document).ready(function() {
      $('#dataConsultor').DataTable( {
          //"order": [[ 8, "asc" ]]
          order: [1, 'asc'],
          "language": {
            "url": "//cdn.datatables.net/plug-ins/1.10.15/i18n/Spanish.json"
          }
      } );
    } );
    $(document).ready(function() {
      var table = $('#dataConsultor').DataTable();
      $('#dataConsultor tbody').on( 'click', 'tr', function () {
          $(this).toggleClass('selected');
      } );
    } );
    
    this.data = JSON.parse(ConsultorPageComponent.datosConsulta);
  }

  atras(){
    ConsultorPageComponent.datosConsulta = null;
    this.router.navigate(['/home']);
  }

  relatorio(){
    this.spinner.show();
    let selectData: Array<string> = [];
    var table = $('#dataConsultor').DataTable();
    selectData = table.rows('.selected').data().toArray();
    if( selectData.length > 0 ){
            const jsonObject = [];
            selectData.forEach(
              item => {
                var jsonItem = {
                  "idCliente": item[0]
                }
                jsonObject.push(jsonItem);
              }
            );
            let data = {
              "data": jsonObject
            }
            this.metrics.getMetrics(data).subscribe(
              response => {
                console.log("Exitoooo");
                console.log(response);
                this.spinner.hide();
              },
              error => {
                console.log("Error");
                console.log(error);
                this.spinner.hide();
              }
            );
    } else {
      this.spinner.hide();
      Swal.fire({
        icon: 'warning',
        text: 'Debe seleccionar mínimo uno (1) registro.'
      })
    }
  }

}