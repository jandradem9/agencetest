import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ConsultorPageComponent } from './consultor-page.component';

describe('ConsultorPageComponent', () => {
  let component: ConsultorPageComponent;
  let fixture: ComponentFixture<ConsultorPageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ConsultorPageComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ConsultorPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
