import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import Swal from 'sweetalert2';
import { ConsultorPageComponent } from '../consultor-page/consultor-page.component';
import { GetConsultorService } from '../services/getConsultor.service';
import { SpinnerService } from '../services/spinner.service';

@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.css'],
  providers: [ConsultorPageComponent]
})
export class HomePageComponent implements OnInit {

  constructor(
    private router: Router,
    private spinner: SpinnerService,
    private service: GetConsultorService
  ){}

  ngOnInit(): void {}

  ingresar(){
    this.spinner.show();
    this.service.detData().subscribe(
      response => {
        if(response.length > 0){
          const jsonObject = [];
          response.forEach(element => {
            var user = {
              "user": element,
            }
            jsonObject.push(user);
          });
          ConsultorPageComponent.datosConsulta = JSON.stringify(jsonObject);
          this.spinner.hide();
          this.router.navigate(['/consultorPage']);
        } else {
          Swal.fire({
            icon: 'warning',
            text: 'No hay datos para mostrar. Por favor intentar de nuevo o comunicarse con el administrador del sistema.',
          })
          this.spinner.hide();
        }
      },
      error => {
        console.log("<<===== Error Inicio ====>>");
        console.log(error);
        console.log("<<===== Error FIN ====>>");
        Swal.fire({
          icon: 'warning',
          text: 'Error en procesar datos. Por favor intentar de nuevo o comunicarse con el administrador del sistema.',
        })
        this.spinner.hide();

      }
    );
  }

}
