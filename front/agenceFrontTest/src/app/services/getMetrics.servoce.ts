import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, Subject } from "rxjs";
import { environment } from '../../environments/environment';
import {timeout} from "rxjs/operators";

@Injectable({
    providedIn: 'root'
})

export class GetMetricsService {

    constructor(
        private http: HttpClient
    ) {}


    getMetrics(data: any){
        const httpOptions = {
            headers: new HttpHeaders({
                'Access-Control-Allow-Methods': '*',
                'Content-Type': 'application/json'
            })
        };
        return this.http.post(environment.getMetrics.url, data, httpOptions).pipe(
            timeout(environment.getMetrics.timeOutMiliSeconds)
        );
    }

}