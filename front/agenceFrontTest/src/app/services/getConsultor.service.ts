import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, Subject } from "rxjs";
import { environment } from '../../environments/environment';
import {timeout} from "rxjs/operators";

@Injectable({
    providedIn: 'root'
})

export class GetConsultorService {

    constructor(
        private http: HttpClient
    ) {}

    detData(): Observable<any> {
        const httpOptions = {
            headers: new HttpHeaders({
                'Access-Control-Allow-Methods': '*',
                'Content-Type': 'application/json'
            })
        };
        return this.http.get(environment.getConsultor.url, httpOptions).pipe(
            timeout(environment.getConsultor.timeOutMiliSeconds)
        );
    }

}