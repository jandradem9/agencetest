import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { HomePageComponent } from './home-page/home-page.component';
import { RouterModule, Routes } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { DataTablesModule } from 'angular-datatables';
import { SpinnerComponent } from './spinner/spinner.component';
import { SpinnerModule } from './spinner/spinner.module';
import { ConsultorPageComponent } from './consultor-page/consultor-page.component';

const appRoutes: Routes = [
  { path: '',  redirectTo: '/home', pathMatch: 'full' },
  { path: 'home', component: HomePageComponent },
  { path: 'consultorPage', component: ConsultorPageComponent }
];
@NgModule({
  declarations: [
    AppComponent,
    HomePageComponent,
    ConsultorPageComponent
  ],
  imports: [
    RouterModule.forRoot(appRoutes),
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    SpinnerModule,
    HttpClientModule,
    DataTablesModule
  ],
  providers: [
    HomePageComponent
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
