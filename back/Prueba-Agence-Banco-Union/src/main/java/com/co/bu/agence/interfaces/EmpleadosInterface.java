package com.co.bu.agence.interfaces;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.co.bu.agence.entity.Empleados;

@Repository
public interface EmpleadosInterface extends JpaRepository<Empleados, Long> {
	
	

}
