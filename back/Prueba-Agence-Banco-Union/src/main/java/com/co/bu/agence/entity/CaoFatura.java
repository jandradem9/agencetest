package com.co.bu.agence.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;

@Entity(name =  "cao_fatura")
@Table(name =  "cao_fatura")
public class CaoFatura {
	
	/*
	`co_fatura` int(8) unsigned NOT NULL auto_increment,
	`co_cliente` int(8) NOT NULL default '0',
	`co_sistema` int(8) NOT NULL default '0',
	`co_os` int(8) NOT NULL default '0',
	`num_nf` int(10) NOT NULL default '0',
	`total` float NOT NULL default '0',
	`valor` float NOT NULL default '0',
	`data_emissao` date NOT NULL default '0000-00-00',
	`corpo_nf` text NOT NULL,
	`comissao_cn` float NOT NULL default '0',
	`total_imp_inc` float NOT NULL default '0',
	PRIMARY KEY  (`co_fatura`)
	ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=164 ;
	*/
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer co_fatura;
	
	@Column(nullable = false)
	private Integer co_cliente = 0;
	
	@Column(nullable = false)
	private Integer co_sistema = 0;
	
	@Column(nullable = false)
	private Integer co_os = 0;
	
	@Column(nullable = false)
	private Integer num_nf = 0;
	
	@Column(nullable = false)
	private float total = 0;
	
	@Column(nullable = false)
	private float valor = 0;
	
	@Column(nullable = false)
	private String data_emissao = "0000-00-00";
	
	@Lob
	@Column(nullable = false)
	private String corpo_nf;
	
	@Column(nullable = false)
	private Integer comissao_cn = 0;
	
	@Column(nullable = false)
	private Integer total_imp_inc = 0;

	public Integer getCo_fatura() {
		return co_fatura;
	}

	public void setCo_fatura(Integer co_fatura) {
		this.co_fatura = co_fatura;
	}

	public Integer getCo_cliente() {
		return co_cliente;
	}

	public void setCo_cliente(Integer co_cliente) {
		this.co_cliente = co_cliente;
	}

	public Integer getCo_sistema() {
		return co_sistema;
	}

	public void setCo_sistema(Integer co_sistema) {
		this.co_sistema = co_sistema;
	}

	public Integer getCo_os() {
		return co_os;
	}

	public void setCo_os(Integer co_os) {
		this.co_os = co_os;
	}

	public Integer getNum_nf() {
		return num_nf;
	}

	public void setNum_nf(Integer num_nf) {
		this.num_nf = num_nf;
	}

	public float getTotal() {
		return total;
	}

	public void setTotal(float total) {
		this.total = total;
	}

	public float getValor() {
		return valor;
	}

	public void setValor(float valor) {
		this.valor = valor;
	}

	public String getData_emissao() {
		return data_emissao;
	}

	public void setData_emissao(String data_emissao) {
		this.data_emissao = data_emissao;
	}

	public String getCorpo_nf() {
		return corpo_nf;
	}

	public void setCorpo_nf(String corpo_nf) {
		this.corpo_nf = corpo_nf;
	}

	public Integer getComissao_cn() {
		return comissao_cn;
	}

	public void setComissao_cn(Integer comissao_cn) {
		this.comissao_cn = comissao_cn;
	}

	public Integer getTotal_imp_inc() {
		return total_imp_inc;
	}

	public void setTotal_imp_inc(Integer total_imp_inc) {
		this.total_imp_inc = total_imp_inc;
	}
}
