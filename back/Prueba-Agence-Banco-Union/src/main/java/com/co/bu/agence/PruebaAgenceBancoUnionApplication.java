package com.co.bu.agence;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import com.co.bu.agence.entity.Empleados;
import com.co.bu.agence.interfaces.EmpleadosInterface;

@SpringBootApplication
public class PruebaAgenceBancoUnionApplication {

	public static void main(String[] args) {
		SpringApplication.run(PruebaAgenceBancoUnionApplication.class, args);
	}

	@Bean
	public CommandLineRunner insertData(EmpleadosInterface repository) {
		return (args) -> {
			// save a few customers
			Empleados empleado0 = new Empleados();
			empleado0.setId((long) 1234567890);
			empleado0.setEmail("jaime.andrade@bancounion.com");
			empleado0.setPassword("Giros123");
			empleado0.setCo_sistema(1);
			empleado0.setStatus("activo");
			
			Empleados empleado1 = new Empleados();
			empleado1.setId((long) 1234567891);
			empleado1.setEmail("arruda_carlos1@hotmail.com");
			empleado1.setPassword("raposa2");
			empleado1.setCo_sistema(2);
			empleado1.setStatus("activo");
			
			
			Empleados empleado2 = new Empleados();
			empleado2.setId((long) 1234567892);
			empleado2.setEmail("arthur@webmotors.com.br");
			empleado2.setPassword("raposa");
			empleado2.setCo_sistema(3);
			empleado2.setStatus("activo");
			
			Empleados empleado3 = new Empleados();
			empleado3.setId((long) 1234567893);
			empleado3.setEmail("ricardo.martins@agence.com.br");
			empleado3.setPassword("raposa");
			empleado3.setCo_sistema(4);
			empleado3.setStatus("activo");
			
			Empleados empleado4 = new Empleados();
			empleado4.setId((long) 1234567894);
			empleado4.setEmail("dbiaggio@toyota.com.br");
			empleado4.setPassword("bgo12pls");
			empleado4.setCo_sistema(5);
			empleado4.setStatus("activo");
			
			Empleados empleado5 = new Empleados();
			empleado5.setId((long) 1234567895);
			empleado5.setEmail("arruda_carlos1@hotmail.com");
			empleado5.setPassword("raposa2");
			empleado5.setCo_sistema(6);
			empleado5.setStatus("activo");	
			
			Empleados empleado6 = new Empleados();
			empleado6.setId((long) 1234567896);
			empleado6.setEmail("abuosi@toyota.com.br");
			empleado6.setPassword("agence");
			empleado6.setCo_sistema(7);
			empleado6.setStatus("activo");
			
			Empleados empleado7 = new Empleados();
			empleado7.setId((long) 1234567897);
			empleado7.setEmail("cristiane.lopes@toyota.com.br");
			empleado7.setPassword("agence");
			empleado7.setCo_sistema(8);
			empleado7.setStatus("activo");
			
			repository.save(empleado0);
			repository.save(empleado1);
			repository.save(empleado2);
			repository.save(empleado3);
			repository.save(empleado4);
			repository.save(empleado5);
			repository.save(empleado6);
			repository.save(empleado7);
			
		};
	}

}
