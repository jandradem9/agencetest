package com.co.bu.agence.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity(name =  "cao_usuario")
@Table(name =  "cao_usuario")
public class CaoUsuario {

	@Id
	@Column(nullable = false, unique=true)
	private String co_usuario;

	@Column(nullable = false)
	private String no_usuario;

	@Column(nullable = false)
	private String ds_senha;
	
	private String co_usuario_autorizacao;
	
	private Integer nu_matricula;
	
	private String dt_nascimento;
	
	private String dt_admissao_empresa;
	
	private String dt_desligamento;
	
	private String dt_inclusao;
	
	private String dt_expiracao;
	
	private String nu_cpf;
	
	private String nu_rg;
	
	private String no_orgao_emissor;
	
	private String uf_orgao_emissor;
	
	private String ds_endereco;
	
	private String no_email;
	
	private String no_email_pessoal;
	
	private String nu_telefone;
	
	@Column(nullable = false)
	private String dt_alteracao;
	
	private String url_foto;
	
	private String instant_messenger;
	
	private Integer icq;
	
	private String msn;
	
	private String yms;
	
	private String ds_comp_end;
	
	private String ds_bairro;
	
	private String nu_cep;
	
	private String no_cidade;
	
	private String uf_cidade;
	
	private String dt_expedicao;

	public String getCo_usuario() {
		return co_usuario;
	}

	public void setCo_usuario(String co_usuario) {
		this.co_usuario = co_usuario;
	}

	public String getNo_usuario() {
		return no_usuario;
	}

	public void setNo_usuario(String no_usuario) {
		this.no_usuario = no_usuario;
	}

	public String getDs_senha() {
		return ds_senha;
	}

	public void setDs_senha(String ds_senha) {
		this.ds_senha = ds_senha;
	}

	public String getCo_usuario_autorizacao() {
		return co_usuario_autorizacao;
	}

	public void setCo_usuario_autorizacao(String co_usuario_autorizacao) {
		this.co_usuario_autorizacao = co_usuario_autorizacao;
	}

	public Integer getNu_matricula() {
		return nu_matricula;
	}

	public void setNu_matricula(Integer nu_matricula) {
		this.nu_matricula = nu_matricula;
	}

	public String getDt_nascimento() {
		return dt_nascimento;
	}

	public void setDt_nascimento(String dt_nascimento) {
		this.dt_nascimento = dt_nascimento;
	}

	public String getDt_admissao_empresa() {
		return dt_admissao_empresa;
	}

	public void setDt_admissao_empresa(String dt_admissao_empresa) {
		this.dt_admissao_empresa = dt_admissao_empresa;
	}

	public String getDt_desligamento() {
		return dt_desligamento;
	}

	public void setDt_desligamento(String dt_desligamento) {
		this.dt_desligamento = dt_desligamento;
	}

	public String getDt_inclusao() {
		return dt_inclusao;
	}

	public void setDt_inclusao(String dt_inclusao) {
		this.dt_inclusao = dt_inclusao;
	}

	public String getDt_expiracao() {
		return dt_expiracao;
	}

	public void setDt_expiracao(String dt_expiracao) {
		this.dt_expiracao = dt_expiracao;
	}

	public String getNu_cpf() {
		return nu_cpf;
	}

	public void setNu_cpf(String nu_cpf) {
		this.nu_cpf = nu_cpf;
	}

	public String getNu_rg() {
		return nu_rg;
	}

	public void setNu_rg(String nu_rg) {
		this.nu_rg = nu_rg;
	}

	public String getNo_orgao_emissor() {
		return no_orgao_emissor;
	}

	public void setNo_orgao_emissor(String no_orgao_emissor) {
		this.no_orgao_emissor = no_orgao_emissor;
	}

	public String getUf_orgao_emissor() {
		return uf_orgao_emissor;
	}

	public void setUf_orgao_emissor(String uf_orgao_emissor) {
		this.uf_orgao_emissor = uf_orgao_emissor;
	}

	public String getDs_endereco() {
		return ds_endereco;
	}

	public void setDs_endereco(String ds_endereco) {
		this.ds_endereco = ds_endereco;
	}

	public String getNo_email() {
		return no_email;
	}

	public void setNo_email(String no_email) {
		this.no_email = no_email;
	}

	public String getNo_email_pessoal() {
		return no_email_pessoal;
	}

	public void setNo_email_pessoal(String no_email_pessoal) {
		this.no_email_pessoal = no_email_pessoal;
	}

	public String getNu_telefone() {
		return nu_telefone;
	}

	public void setNu_telefone(String nu_telefone) {
		this.nu_telefone = nu_telefone;
	}

	public String getDt_alteracao() {
		return dt_alteracao;
	}

	public void setDt_alteracao(String dt_alteracao) {
		this.dt_alteracao = dt_alteracao;
	}

	public String getUrl_foto() {
		return url_foto;
	}

	public void setUrl_foto(String url_foto) {
		this.url_foto = url_foto;
	}

	public String getInstant_messenger() {
		return instant_messenger;
	}

	public void setInstant_messenger(String instant_messenger) {
		this.instant_messenger = instant_messenger;
	}

	public Integer getIcq() {
		return icq;
	}

	public void setIcq(Integer icq) {
		this.icq = icq;
	}

	public String getMsn() {
		return msn;
	}

	public void setMsn(String msn) {
		this.msn = msn;
	}

	public String getYms() {
		return yms;
	}

	public void setYms(String yms) {
		this.yms = yms;
	}

	public String getDs_comp_end() {
		return ds_comp_end;
	}

	public void setDs_comp_end(String ds_comp_end) {
		this.ds_comp_end = ds_comp_end;
	}

	public String getDs_bairro() {
		return ds_bairro;
	}

	public void setDs_bairro(String ds_bairro) {
		this.ds_bairro = ds_bairro;
	}

	public String getNu_cep() {
		return nu_cep;
	}

	public void setNu_cep(String nu_cep) {
		this.nu_cep = nu_cep;
	}

	public String getNo_cidade() {
		return no_cidade;
	}

	public void setNo_cidade(String no_cidade) {
		this.no_cidade = no_cidade;
	}

	public String getUf_cidade() {
		return uf_cidade;
	}

	public void setUf_cidade(String uf_cidade) {
		this.uf_cidade = uf_cidade;
	}

	public String getDt_expedicao() {
		return dt_expedicao;
	}

	public void setDt_expedicao(String dt_expedicao) {
		this.dt_expedicao = dt_expedicao;
	}
}
