package com.co.bu.agence.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.co.bu.agence.LoadProperties;
import com.co.bu.agence.entity.Empleados;
import com.co.bu.agence.entity.IdCliente;
import com.co.bu.agence.entity.RequestMetrics;
import com.co.bu.agence.interfaces.PruebaAgenceBancoUnion;

@RestController
@RequestMapping("/")
@CrossOrigin(origins = "*", methods= {RequestMethod.GET,RequestMethod.POST})
public class PruebaAgenceBancoUnionController {
	
	@Autowired
	LoadProperties properties;
	
	@Autowired
	PruebaAgenceBancoUnion service;
	
	@RequestMapping(value = "getAllUsers", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Object> getAllUsers() throws Exception {
		
		List<Empleados> data = service.findAllUsers();
		if(data.isEmpty()) {
			return new ResponseEntity<>(null, HttpStatus.OK);
		} else {
			return new ResponseEntity<>(data, HttpStatus.OK);
		}
	}
	
	@RequestMapping(value = "getConsultor", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Object> getConsultor() throws Exception {
		
		List<String> data = service.findConsultor();
		if(data.isEmpty()) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		} else {
			return new ResponseEntity<>(data, HttpStatus.OK);
		}
	}
	
	
	@RequestMapping(value = "getMetrics", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Object> getMetrics(@RequestBody RequestMetrics request) throws Exception {
		if(request.getData().length > 0) {
			return null;	
		} else {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	//

}
