package com.co.bu.agence.interfaces;

import java.util.List;

import com.co.bu.agence.entity.CaoUsuario;
import com.co.bu.agence.entity.Empleados;

public interface PruebaAgenceBancoUnion {

	List<Empleados> findAllUsers();

	List<String> findConsultor();

}
