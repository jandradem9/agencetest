package com.co.bu.agence.entity;

public class ResultadoCaoFatura {
	
	private double receitaLiquida;
	private double custoFixo;
	private double comissao;
	private double lucro;
	public double getReceitaLiquida() {
		return receitaLiquida;
	}
	public void setReceitaLiquida(double receitaLiquida) {
		this.receitaLiquida = receitaLiquida;
	}
	public double getCustoFixo() {
		return custoFixo;
	}
	public void setCustoFixo(double custoFixo) {
		this.custoFixo = custoFixo;
	}
	public double getComissao() {
		return comissao;
	}
	public void setComissao(double comissao) {
		this.comissao = comissao;
	}
	public double getLucro() {
		return lucro;
	}
	public void setLucro(double lucro) {
		this.lucro = lucro;
	}
}
