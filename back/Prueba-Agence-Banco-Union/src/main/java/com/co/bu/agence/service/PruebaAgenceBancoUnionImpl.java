package com.co.bu.agence.service;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.co.bu.agence.LoadProperties;
import com.co.bu.agence.entity.Empleados;
import com.co.bu.agence.interfaces.EmpleadosInterface;
import com.co.bu.agence.interfaces.PruebaAgenceBancoUnion;

@Service
public class PruebaAgenceBancoUnionImpl implements PruebaAgenceBancoUnion {

	@Autowired
	LoadProperties properties;

	@Autowired
	EmpleadosInterface repository;
	
	@PersistenceContext
	private EntityManager em;

	@Override
	public List<Empleados> findAllUsers() {
		try {
			return repository.findAll();
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<String> findConsultor() {
		
		List<String> resultList = new ArrayList<String>();
		
		try {
			TypedQuery<String> query = (TypedQuery<String>) em.createQuery(
					"SELECT CU.no_usuario FROM cao_usuario CU INNER JOIN permissao_sistema PS "
					+ "ON CU.co_usuario = PS.co_usuario WHERE PS.co_sistema = 1 and PS.in_ativo = 'N' "
					+ "and PS.co_tipo_usuario IN (0, 1, 2) ORDER BY CU.no_usuario ASC");
			resultList = query.getResultList();
		    em.close();
		    return resultList;
		} catch (Exception e) {
			em.close();
			e.printStackTrace();
			return resultList;
		}
	}

}
