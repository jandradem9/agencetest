package com.co.bu.agence.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity(name =  "permissao_sistema")
@Table(name =  "permissao_sistema")
public class PermissaoSistema implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@Column(nullable = false)
	private String co_usuario;
	
	@Id
	@Column(nullable = false)
	private Integer co_tipo_usuario;
	
	@Id
	@Column(nullable = false)
	private Integer co_sistema;

	@Column(nullable = false)
	private String in_ativo;
	
	private String co_usuario_atualizacao;
	
	@Column(nullable = false)
	private String dt_atualizacao;

	public String getCo_usuario() {
		return co_usuario;
	}

	public void setCo_usuario(String co_usuario) {
		this.co_usuario = co_usuario;
	}

	public Integer getCo_tipo_usuario() {
		return co_tipo_usuario;
	}

	public void setCo_tipo_usuario(Integer co_tipo_usuario) {
		this.co_tipo_usuario = co_tipo_usuario;
	}

	public Integer getCo_sistema() {
		return co_sistema;
	}

	public void setCo_sistema(Integer co_sistema) {
		this.co_sistema = co_sistema;
	}

	public String getIn_ativo() {
		return in_ativo;
	}

	public void setIn_ativo(String in_ativo) {
		this.in_ativo = in_ativo;
	}

	public String getCo_usuario_atualizacao() {
		return co_usuario_atualizacao;
	}

	public void setCo_usuario_atualizacao(String co_usuario_atualizacao) {
		this.co_usuario_atualizacao = co_usuario_atualizacao;
	}

	public String getDt_atualizacao() {
		return dt_atualizacao;
	}

	public void setDt_atualizacao(String dt_atualizacao) {
		this.dt_atualizacao = dt_atualizacao;
	}
}
